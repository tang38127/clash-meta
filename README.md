# Clash Meta 模板

这是一个基于 Clash Meta 订阅的配置文件模板

## 使用方法

- 博客地址：https://blog.misaka.rest/2023/09/15/clash-meta-gl-config/
- 视频教程：https://youtu.be/xh6P5Y_59zw

## 赞助

爱发电：https://afdian.net/a/Misaka-blog

![afdian-MisakaNo の 小破站](https://user-images.githubusercontent.com/122191366/211533469-351009fb-9ae8-4601-992a-abbf54665b68.jpg)